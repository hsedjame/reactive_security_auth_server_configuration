package io.gitlab.hsedjame.project.security.auth.server.configuration.annotations;

import io.gitlab.hsedjame.project.security.auth.server.configuration.config.CorsFilter;
import io.gitlab.hsedjame.project.security.auth.server.configuration.config.WebFluxSecurityConfig;
import io.gitlab.hsedjame.project.security.auth.server.configuration.security.ReactiveAuthServerAuthenticationManager;
import io.gitlab.hsedjame.project.security.core.configuration.beans.BeanProvider;
import io.gitlab.hsedjame.project.security.core.configuration.security.SecurityContextRepository;
import io.gitlab.hsedjame.project.security.core.exceptions.ConnectionExceptionMessages;
import io.gitlab.hsedjame.project.security.core.jwt.JwtProperties;
import io.gitlab.hsedjame.project.security.core.jwt.JwtUtils;
import io.gitlab.hsedjame.project.security.core.services.MyaldocReactiveUserDetailsService;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;


@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({BeanProvider.class,
        ConnectionExceptionMessages.class,
        MyaldocReactiveUserDetailsService.class,
        SecurityContextRepository.class,
        ReactiveAuthServerAuthenticationManager.class,
        WebFluxSecurityConfig.class,
        CorsFilter.class,
        JwtUtils.class,
        JwtProperties.class})
public @interface EnableReactiveAuthServerSecurity {
}
