package io.gitlab.hsedjame.project.security.auth.server.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"io.gitlab.hsedjame"})
public class ProjectSecurityAuthServerConfiguraionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSecurityAuthServerConfiguraionApplication.class, args);
    }

}
